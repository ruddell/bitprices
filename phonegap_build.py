#!/usr/bin/python
import subprocess
import time
import os
import xml.etree.ElementTree as ET
import sys

#TODO Change to API key from phonegap

#setup phonegap vars
phonegapUser = "jonathan.ruddell@gmail.com"

#pass the password as a line argument
phonegapPass = sys.argv[1]
phonegapAppId = 1664825
phonegapAppUrl = "https://build.phonegap.com/api/v1/apps/" + str(phonegapAppId)

#options are android, ios and winphone
platforms = ["android"]

#setup directory for apps
subprocess.call(("mkdir apps"), shell=True)

# Get the version from config.xml
root = ET.parse('config.xml').getroot()
name = root[0].text
version = root.attrib['version']
fileName = name + '-' + version + '.zip'


subprocess.call(('curl -u %s:%s -X PUT -d \'data={"pull":"true"}\' %s') % (phonegapUser, phonegapPass, phonegapAppUrl), shell=True)

# Create a directory for the apps to be stored in
subprocess.call(("mkdir %s") % ("apps/" + version), shell=True)

# Sleep for 60 seconds to give Phonegap time to build
time.sleep(60)

# Download the apps and store them in the app/version folder
for platform in platforms:
    appUrl = phonegapAppUrl + "/" + platform
    extension = ""
    if platform == "android":
        extension = ".apk"
    elif platform == "winphone":
        extension = ".xap"
    elif platform == "ios":
        extension = ".ipa"

    appFolder = "./apps/" + version + "/"
    appFileName = name + "-" + platform + "-" + version + extension

    subprocess.call(("curl -u %s:%s -o %s%s -L %s")
                    % (phonegapUser, phonegapPass, appFolder, appFileName, appUrl), shell=True)



