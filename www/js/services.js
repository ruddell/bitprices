angular.module('bitprices.services', [])
    .factory('PriceService', function($http, $q) {
        var baseUrl = "http://bp.jruddell.com:5000/";
        return {
            getPrices: function() {
                var deferred = $q.defer();
                $http.get(baseUrl + "prices/").
                    success(function(data) {
                        deferred.resolve(data);
                    });
                return deferred.promise;
            },
            getSite: function(site_name) {
                var deferred = $q.defer();
                $http.get(baseUrl + "prices/" + site_name).
                    success(function (data) {
                        deferred.resolve(data);
                    });
                return deferred.promise;
            },
            getStats: function() {
                var deferred = $q.defer();
                $http.get(baseUrl + "stats/").
                    success(function (data) {
                        deferred.resolve(data);
                    });
                return deferred.promise;
            },
            getChart: function(site, time) {
                var deferred = $q.defer();
                $http.get(baseUrl + "chart/avgMinMax/" + site + "?period="+time).
                    success(function (data) {
                        deferred.resolve(data);
                    });
                return deferred.promise;
            },
            getStatChart: function(stat, time) {
                var deferred = $q.defer();
                $http.get(baseUrl + "chart/stat/" + stat + "?period="+time).
                    success(function (data) {
                        deferred.resolve(data);
                    });
                return deferred.promise;
            },
            getVariability: function(site) {
                var deferred = $q.defer();
                $http.get(baseUrl + "variability/"+site).
                    success(function (data) {
                        deferred.resolve(data);
                    });
                return deferred.promise;
            }
        };

    })
    .service('ChartGenerator', function(PriceService, $q) {
        var chartInfo = {};
        return {
            getChartInfo: function(site, period){
                var deferred = $q.defer();
                period = period.replace(" ", "").toLowerCase();
                PriceService.getChart(site,period).then(function (data) {
                    var avg = [];
                    var max = [];
                    var min = [];
                    var time = [];
                    var lastAvg = 0;
                    var lastMin = 0;
                    var lastMax = 0;
                    angular.forEach(data.chartData, function(data){
                            if(isNaN(data.avg)){
                                data.avg = lastAvg;
                                data.min = lastMin;
                                data.max = lastMax;
                            }else{
                                lastMin = parseFloat(data.min);
                                lastAvg = parseFloat(data.avg);
                                lastMax = parseFloat(data.max);
                            }
                            avg.push(parseFloat(data.avg));
                            min.push(parseFloat(data.min));
                            max.push(parseFloat(data.max));
                            var date = new Date(data.time);
                            //set up bottom labels
                            var formattedDate = "";
                            switch(period) {
                                case "day":
                                    formattedDate = date.getHours() + ":" +  ('0'+date.getMinutes()).slice(-2);
                                    break;
                                case "week":
                                    formattedDate = (parseInt(date.getMonth())+1) + "-" + date.getDate() + " " + date.getHours() + ":" +  ('0'+date.getMinutes()).slice(-2);
                                    break;
                                case "month":
                                    formattedDate = (parseInt(date.getMonth())+1) + "-" + date.getDate();
                                    break;
                                case "threemonth":
                                    formattedDate = (parseInt(date.getMonth())+1) + "-" + date.getDate();
                                    break;
                                case "sixmonth":
                                    formattedDate = (parseInt(date.getMonth())+1) + "-" + date.getDate();
                                    break;
                                case "year":
                                    formattedDate = (parseInt(date.getMonth())+1) + "-" + date.getDate();
                                    break;

                                default:
                                    formattedDate = date.getHours() + ":" +  ('0'+date.getMinutes()).slice(-2);
                            }
                            time.push(formattedDate);

                    });
                    var labels = time;
                    var series = ['Max', 'Avg', 'Min'];
                    var dataForSeries = [max, avg, min];
                    var info = [labels, series, dataForSeries];
                    deferred.resolve(info);
                });
                return deferred.promise;

            },
            getStatChart: function(stat, period){
                var deferred = $q.defer();
                PriceService.getStatChart(stat, period).then(function(data){
                    var dataPoints = [];
                    var time = [];
                    var lastPoint = 0;
                    angular.forEach(data.chartData, function(data){

                            //if a piece of data is missing, then substitute the last piece

                            if(isNaN(data.stat)){
                                data.point = lastPoint;
                            }else if(data.stat.length > 12){
                                data.stat = data.stat/100000000
                            }
                            else{
                                lastPoint = parseFloat(data.stat)   ;
                            }

                            dataPoints.push(parseFloat(data.stat));
                            var date = new Date(parseFloat(data.time)*1000);

                            //set up bottom labels
                            var formattedDate = date.getHours() + ":" + date.getMinutes();
                            switch(period) {
                                case "day":
                                    formattedDate = date.getHours() + ":" + date.getMinutes();
                                    break;
                                case "week":
                                    formattedDate = (parseInt(date.getMonth())+1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes();
                                    break;
                                case "month":
                                    formattedDate = (parseInt(date.getMonth())+1) + "-" + date.getDate();
                                    break;
                                case "threemonth":
                                    formattedDate = (parseInt(date.getMonth())+1) + "-" + date.getDate();
                                    break;
                                case "sixmonth":
                                    formattedDate = (parseInt(date.getMonth())+1) + "-" + date.getDate();
                                    break;
                                case "year":
                                    formattedDate = date.getHours() + ":" + date.getMinutes();
                                    break;

                                default:
                                    formattedDate = date.getHours() + ":" + date.getMinutes();
                            }
                            time.push(formattedDate);

                    });
                    var labels = time;
                    var series = [stat];
                    var dataSet = [dataPoints];
                    var info = [labels, series, dataSet];

                    deferred.resolve(info);
                });
                return deferred.promise;

            }
        };
    })

    .constant('DEFAULT_SETTINGS', [
        { name: "Bitstamp", checked: true, type: "site" },
        { name: "Coinbase", checked: true, type: "site" },
        { name: "Kraken",   checked: true, type: "site" },
        { name: "Bitfinix", checked: true, type: "site" },
        { name: "BTC-E",    checked: true, type: "site" },
        { name: "BTCChina", checked: true, type: "site" },
        { name: "OKCoin",   checked: true, type: "site" },
        { name: "itBit",    checked: true, type: "site" },
        { name: "Average",  checked: true, type: "site" },
        { name: "time",     value: "Day",  type: "preference" }

    ])

    .factory('Settings', function($rootScope, DEFAULT_SETTINGS) {
        var _settings = {};
        try {
            _settings = JSON.parse(window.localStorage['settings']);
        } catch(e) {
        }

        // Just in case we have new settings that need to be saved
        _settings = angular.extend({}, DEFAULT_SETTINGS, _settings);

        if(!_settings) {
            window.localStorage['settings'] = JSON.stringify(_settings);
        }

        var obj = {
            getSettings: function() {
                return _settings;
            },
            // Save the settings to localStorage
            save: function() {
                window.localStorage['settings'] = JSON.stringify(_settings);
                $rootScope.$broadcast('settings.changed', _settings);
            },
            // Get a settings val
            get: function(k) {
                return _settings[k];
            },
            // Set a settings val
            set: function(k, v) {
                _settings[k] = v;
                this.save();
            },

            getChartPeriod: function() {
                return _settings[9].value;
            }
        };

        // Save the settings to be safe
        obj.save();
        return obj;
    });