angular.module('bitprices.filters', [])
    //removes the first hyphen from a string
    .filter('noDash', function () {
        return function (text) {
            return text.replace(/-/, '');
        };
    });