angular.module('bitprices.controllers', [])

    .controller('SettingsCtrl', function($scope, Settings) {
        $scope.settings = Settings.getSettings();

        // Watch deeply for settings changes, and save them
        // if necessary
        $scope.$watch('settings', function(v) {
            Settings.save();
        }, true);

        $scope.closeSettings = function() {
            $scope.modal.hide();

        }})

    .controller('DashCtrl', function($scope, PriceService, $ionicModal, Settings) {
        $scope.showSettings = function() {
            if(!$scope.settingsModal) {
                // Load the modal from the given template URL
                $ionicModal.fromTemplateUrl('../templates/settings.html', function(modal) {
                    $scope.settingsModal = modal;
                    $scope.settingsModal.show();
                }, {
                    // The animation we want to use for the modal entrance
                    animation: 'slide-in-up'
                });
            } else {
                $scope.settingsModal.show();
            }
        };


        $scope.doRefresh = function() {
            $scope.settings = Settings.getSettings();
            PriceService.getPrices().then(function(data){
                $scope.pricedata = data;
            }).finally(function() {
                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');
            });
        };
    })

    .controller('StatsCtrl', function($scope, PriceService) {
        $scope.doRefresh = function() {
            PriceService.getStats().then(function(data){
                $scope.stats = data;
            })
        };
    })

    .controller('ExchangeDetailCtrl', function($scope, $stateParams, PriceService, $filter, ChartGenerator, Settings) {
        $scope.site = ($stateParams.siteId) ? $filter('noDash')($stateParams.siteId) : "Average";
        $scope.timeperiod = Settings.getChartPeriod();
        $scope.doRefresh = function() {
            PriceService.getSite($scope.site).then(function (data) {
                $scope.price = data.prices;
            });
            $scope.updateChartPeriod($scope.timeperiod);
            PriceService.getVariability($scope.site).then(function (data) {
                $scope.variability = data.variability[0];
                $scope.currentprice = data.currentPrice;
            }).finally(function() {
                    // Stop the ion-refresher from spinning
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };
        $scope.updateChartPeriod = function(period) {
            $scope.timeperiod = period;
            period = period.replace(" ", "").toLowerCase();
            ChartGenerator.getChartInfo($scope.site, period).then(function(data){
                $scope.labels = data[0];
                $scope.series = data[1];
                $scope.data = data[2];
            });
        };
    })
    .controller('ChartCtrl', function($scope, $stateParams, PriceService, ChartGenerator, Settings) {
        $scope.time = Settings.getChartPeriod();

        $scope.updateChartPeriod = function(period) {
            $scope.time = period;
            period = period.replace(" ", "").toLowerCase();
            ChartGenerator.getChartInfo("Average", period).then(function(data){
                $scope.labels = data[0];
                $scope.series = data[1];
                $scope.data = data[2];

            });
        };
        $scope.updateChartPeriod($scope.time);
    })
    .controller('StatsChartCtrl', function($scope, $stateParams, PriceService, ChartGenerator, Settings) {
        $scope.type = $stateParams.statId ? $stateParams.statId : "Volume";
        $scope.time = Settings.getChartPeriod();


        $scope.updateChartPeriod = function(period) {
            $scope.time = period;
            period = period.replace(" ", "").toLowerCase();
            ChartGenerator.getStatChart($scope.type, period).then(function(data){
                $scope.labels = data[0];
                $scope.series = data[1];
                $scope.data = data[2];
            });

        };
        $scope.updateChartPeriod($scope.time);
    });