angular.module('bitprices', ['ionic', 'angular-cache', 'bitprices.controllers', 'bitprices.services', 'bitprices.filters','chart.js'])

    .run(function($ionicPlatform, $rootScope, $ionicLoading) {
      $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleLightContent();
        }
      });
      $rootScope.$on('loading:show', function() {
        $ionicLoading.show({template: '<ion-spinner></ion-spinner>'})
      });

      $rootScope.$on('loading:hide', function() {
        $ionicLoading.hide()
      });
    })

    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {

      $stateProvider

          .state('tab', {
            url: "/tab",
            abstract: true,
            templateUrl: "templates/tabs.html"
          })

        // Each tab has its own nav history stack:

          .state('tab.dash', {
            url: '/dash',
            views: {
              'tab-dash': {
                templateUrl: 'templates/tab-dash.html',
                controller: 'DashCtrl'
              }
            }
          })

          .state('tab.settings', {
            url: '/settings',
            views: {
              'tab-dash': {
                templateUrl: 'templates/settings.html',
                controller: 'SettingsCtrl'
              }
            }
          })

          .state('tab.stats', {
            url: '/stats',
            views: {
              'tab-stats': {
                templateUrl: 'templates/tab-stats.html',
                controller: 'StatsCtrl'
              }
            }
          })
          .state('tab.stats-chart', {
            url: '/stats/:statId',
            views: {
              'tab-stats': {
                templateUrl: 'templates/tab-chart.html',
                controller: 'StatsChartCtrl'
              }
            }
          })
          .state('tab.exchange-detail', {
            url: '/site/:siteId',
            views: {
              'tab-dash': {
                templateUrl: 'templates/exchange-detail.html',
                controller: 'ExchangeDetailCtrl'
              }
            }
          })

          .state('tab.chart', {
            url: '/chart',
            views: {
              'tab-chart': {
                templateUrl: 'templates/tab-chart.html',
                controller: 'ChartCtrl'
              }
            }
          });

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/tab/dash');

      //show loading screen when waiting for data
      $httpProvider.interceptors.push(function($rootScope) {
        return {
          request: function(config) {
            $rootScope.$broadcast('loading:show')
            return config
          },
          response: function(response) {
            $rootScope.$broadcast('loading:hide')
            return response
          }
        }
      })

    });
