BitPrices App
=====================

[Most Up-To-Date Android APK] <br />
[Google Play Store Link] <br />
[In-Browser Demo]


This app is built with [Ionic Framework] and compiled through [Adobe Phonegap]. The goal of this app is to provide an easy way to check Bitcoin prices, view other stats, and more.
It is open source so that anyone can ensure nothing malicious is happening in the background.

Price data is gathered from each exchange and stored into a database once every minute.

## To Do List:
Settings, Alerts

## Issues:

## How to Run
To run and change the code locally requires node, npm, cordova and ionic
```
npm install -g cordova ionic
```
Afterwards, clone the repository and run the following to download dependencies
```
npm install
bower install
```
Running the following will then launch a live-updating version locally.
```
ionic serve
```

To run the tests, use the following command
```
karma start
```

## How to Help
Any changes can be pull requested and then implemented into the application after a quick review.


[In-Browser Demo]: http://bp.jruddell.com/
[Google Play Store Link]: https://play.google.com/store/apps/details?id=com.jruddell.bitprices
[Most Up-To-Date Android APK]: https://build.phonegap.com/apps/1664825/share
[Ionic Framework]: http://ionicframework.com/
[Adobe Phonegap]: http://phonegap.com/
