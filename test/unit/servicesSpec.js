describe('Settings tests', function () {
    var Settings;

    beforeEach(function () {
        module('bitprices');

        inject(function (_Settings_) {
            localStorage.clear();
            Settings = _Settings_;
        });
    });

    it('should have a set function', function () {
        expect(angular.isFunction(Settings.set)).toBe(true);
    });
    it('should have a get function', function () {
        expect(angular.isFunction(Settings.get)).toBe(true);
    });
    it('should have a getSettings function', function () {
        expect(angular.isFunction(Settings.getSettings)).toBe(true);
    });
    it('should have a save function', function () {
        expect(angular.isFunction(Settings.save)).toBe(true);
    });
    it('should have a getChartPeriod function', function () {
        expect(angular.isFunction(Settings.getChartPeriod)).toBe(true);
    });

//    todo: add Settings tests
});

describe('PriceService tests', function ($httpBackend) {
    var PriceService, httpMock;
    beforeEach(function () {
        module('bitprices');

        inject(function (_PriceService_, _$httpBackend_) {
            PriceService = _PriceService_;
            $httpBackend = _$httpBackend_;
        });
    });

    it('should have a getPrices function', function () {
        expect(angular.isFunction(PriceService.getPrices)).toBe(true);
    });
    it('should have a getSite function', function () {
        expect(angular.isFunction(PriceService.getSite)).toBe(true);
    });
    it('should have a getStats function', function () {
        expect(angular.isFunction(PriceService.getStats)).toBe(true);
    });
    it('should have a getChart function', function () {
        expect(angular.isFunction(PriceService.getChart)).toBe(true);
    });
    it('should have a getStatChart function', function () {
        expect(angular.isFunction(PriceService.getStatChart)).toBe(true);
    });
    it('should have a getVariability function', function () {
        expect(angular.isFunction(PriceService.getVariability)).toBe(true);
    });
});

describe('ChartGenerator tests', function ($httpBackend) {
    var PriceService, httpMock;
    beforeEach(function () {
        module('bitprices');

        inject(function (_ChartGenerator_, _$httpBackend_) {
            ChartGenerator = _ChartGenerator_;
            $httpBackend = _$httpBackend_;
        });
    });

    it('should have a getChartInfo function', function () {
        expect(angular.isFunction(ChartGenerator.getChartInfo)).toBe(true);
    });
    it('should have a getStatChart function', function () {
        expect(angular.isFunction(ChartGenerator.getStatChart)).toBe(true);
    });
});
