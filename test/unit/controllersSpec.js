describe('SettingsCtrl Test', function() {
    var scope, controller, SettingsService;
    // Load the module
    beforeEach(function () {
        module('bitprices');
    });
    beforeEach(inject(function ($rootScope, $controller, Settings) {
        scope = $rootScope.$new();
        localStorage.clear();
        SettingsService = Settings;
        controller = $controller('SettingsCtrl', {
            $scope: scope
        });
    }));

    it('should have the SettingsCtrl controller', inject(function () {
        expect(controller).toBeDefined();
    }));
    it('should have a closeSettings function', inject(function() {
        expect(angular.isFunction(scope.closeSettings)).toBe(true);
    }));
    it('should load settings from localStorage on load', inject(function() {
        expect(scope.settings).not.toBe(null);
    }));
});

describe('DashCtrl Test', function() {
    var scope, controller;

    // Load the module
    beforeEach(function () {
        module('bitprices');
    });

    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();
        localStorage.clear();
        controller = $controller('DashCtrl', {
            $scope: scope
        });
    }));

    it('should have the DashCtrl controller', inject(function() {
        expect(controller).toBeDefined();
    }));
    it('should have a doRefresh function', inject(function() {
        expect(angular.isFunction(scope.doRefresh)).toBe(true);
    }));
    it('should have a showSettings function', inject(function() {
        expect(angular.isFunction(scope.showSettings)).toBe(true);
    }));
    it('should load settings into the scope', inject(function() {
        expect(scope.settings).not.toBe(null);
        expect(localStorage.settings).not.toBe(null);
    }));
});
describe('StatsCtrl Test', function() {
    var scope, controller;

    // Load the module
    beforeEach(function () {
        module('bitprices');
    });

    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();
        controller = $controller('StatsCtrl', {
            $scope: scope
        });
    }));

    it('should have the StatsCtrl controller', inject(function() {
        expect(controller).toBeDefined();
    }));
    it('should have a doRefresh function', inject(function() {
        expect(angular.isFunction(scope.doRefresh)).toBe(true);
    }));
    it('should load stats into the scope', inject(function() {
        expect(scope.stats).not.toBe(null);
    }));
});


describe('ExchangeDetailCtrl Test', function() {
    var scope, controller;

    // Load the module
    beforeEach(function () {
        module('bitprices');
    });

    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();
        controller = $controller('ExchangeDetailCtrl', {
            $scope: scope
        });
    }));

    it('should have the ExchangeDetailCtrl controller', inject(function() {
        expect(controller).toBeDefined();
    }));
    it('should have a default site of Average', inject(function() {
        expect(scope.site).toBe("Average");
    }));
    it('should have a default time period of Day', inject(function() {
        expect(scope.timeperiod).toBe("Day");
    }));
    it('should have a updateChartPeriod function', inject(function() {
        expect(angular.isFunction(scope.updateChartPeriod)).toBe(true);
    }));
    it('updateChartPeriod function sets data, series, and labels', inject(function() {
        scope.updateChartPeriod("Day");
        expect(scope.data).not.toBe(null);
        expect(scope.series).not.toBe(null);
        expect(scope.labels).not.toBe(null);

    }));
    it('should have a doRefresh function', inject(function() {
        expect(angular.isFunction(scope.doRefresh)).toBe(true);
    }));
    it('doRefresh function sets price, currentprice, and variability', inject(function() {
        scope.updateChartPeriod("Day");
        expect(scope.price).not.toBe(null);
        expect(scope.variability).not.toBe(null);
        expect(scope.currentprice).not.toBe(null);

    }));
});



describe('ChartCtrl Test', function() {
    var scope, controller;
    // Load the module
    beforeEach(function () {
        module('bitprices');
    });
    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();
        controller = $controller('ChartCtrl', {
            $scope: scope
        });
    }));

    it('should have the ChartCtrl controller', inject(function() {
        expect(controller).toBeDefined();
    }));
    it('should autoload a chart', inject(function() {
        expect(scope.data).not.toBe(null);
        expect(scope.series).not.toBe(null);
        expect(scope.labels).not.toBe(null);
    }));
    it('should have a default time period of Day', inject(function() {
        expect(scope.time).toBe("Day");
    }));
    it('should have a updateChartPeriod function', inject(function() {
        expect(angular.isFunction(scope.updateChartPeriod)).toBe(true);
    }));
    it('updateChartPeriod function sets data, series, and labels', inject(function() {
        scope.updateChartPeriod("Day");
        expect(scope.data).not.toBe(null);
        expect(scope.series).not.toBe(null);
        expect(scope.labels).not.toBe(null);

    }));
});
describe('StatsChartCtrl Test', function() {
    var scope, controller;
    // Load the module
    beforeEach(function () {
        module('bitprices');
    });

    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();
        controller = $controller('StatsChartCtrl', {
            $scope: scope
        });
    }));

    it('should have the StatsChartCtrl controller', inject(function() {
        expect(controller).toBeDefined();
    }));
    it('should autoload a chart', inject(function() {
        expect(scope.data).not.toBe(null);
        expect(scope.series).not.toBe(null);
        expect(scope.labels).not.toBe(null);
    }));
    it('should have a updateChartPeriod function', inject(function() {
        expect(angular.isFunction(scope.updateChartPeriod)).toBe(true);
    }));
    it('should have a default stat of Volume', inject(function() {
        expect(scope.type).toBe("Volume");
    }));
    it('should have a default time period of Day', inject(function() {
        expect(scope.time).toBe("Day");
    }));
    it('updateChartPeriod function sets data, series, and labels', inject(function() {
        scope.updateChartPeriod("Day");
        expect(scope.data).not.toBe(null);
        expect(scope.series).not.toBe(null);
        expect(scope.labels).not.toBe(null);
    }));
});

