describe('noDash Filter Test', function() {

    var fixtures = [
        'Test-ExchangeName',
        'TestE-xchangeName',
        'TestEx-changeName',
        'TestExc-hangeName',
        'TestExch-angeName',
        'TestExcha-ngeName',
        'TestExchan-geName',
        'TestExchang-eName',
    ];

    beforeEach(module('bitprices.filters'));

    it('should remove the first hypehn from strings correctly', inject(function(noDashFilter) {

        fixtures.forEach(function(fixture) {
            expect(noDashFilter(fixture)).toEqual('TestExchangeName');
        });

    }));

    it('should return an empty string when a value is not passed', inject(function(noDashFilter) {
        expect(noDashFilter("")).toEqual('');
    }));
});
